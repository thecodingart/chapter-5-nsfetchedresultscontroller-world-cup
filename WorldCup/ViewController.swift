//
//  ViewController.swift
//  WorldCup
//
//  Created by Pietro Rea on 8/2/14.
//  Copyright (c) 2014 Razeware. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var coreDataStack: CoreDataStack!
    var fetchResultsController: NSFetchedResultsController!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let fetchRequest = NSFetchRequest(entityName: "Team")
        
        //The first sort descriptor should match the sectionNameKeyPath
        let zoneSort = NSSortDescriptor(key: "qualifyingZone", ascending: true)
        let scoreSort = NSSortDescriptor(key: "wins", ascending: false)
        let nameSort = NSSortDescriptor(key: "teamName", ascending: true)
        fetchRequest.sortDescriptors = [zoneSort, scoreSort, nameSort]
        
        fetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: coreDataStack.context, sectionNameKeyPath: "qualifyingZone", cacheName: nil)
        
        var error: NSError? = nil
        if (!fetchResultsController.performFetch(&error)) {
            println("Error: \(error?.localizedDescription)")
        }
    }
    
    func numberOfSectionsInTableView
        (tableView: UITableView) -> Int {
            
            return fetchResultsController.sections!.count
    }
    
    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
            let sectionInfo = fetchResultsController.sections![section] as NSFetchedResultsSectionInfo
            
            return sectionInfo.numberOfObjects
    }
    
    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
            
            let resuseIdentifier = "teamCellReuseIdentifier"
            
            var cell =
            tableView.dequeueReusableCellWithIdentifier(
                resuseIdentifier, forIndexPath: indexPath)
                as TeamCell
            
            configureCell(cell, indexPath: indexPath)
            
            return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionInfo = fetchResultsController.sections![section] as NSFetchedResultsSectionInfo
        return sectionInfo.name
    }
    
    func configureCell(cell: TeamCell, indexPath: NSIndexPath) {
        let team = fetchResultsController.objectAtIndexPath(indexPath) as Team
        
        cell.flagImageView.image = UIImage(named: team.imageName)
        cell.teamLabel.text = team.teamName
        cell.scoreLabel.text = "Wins: \(team.wins)"
    }
    
    func tableView(tableView: UITableView,
        didSelectRowAtIndexPath indexPath: NSIndexPath) {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            let team = fetchResultsController.objectAtIndexPath(indexPath) as Team
            let wins = team.wins.integerValue
            team.wins = NSNumber(integer: wins + 1)
            coreDataStack.saveContext()
            
            tableView.reloadData()
    }
}

